# README #

Este proyecto realizado con el fin de ejecutar las diferentes características del lenguaje Typescript entre el grupo. Decidimos mantener el mismo rol de inventarios de empresa de moda de ropa.

Fue ejecutado este trabajo de la siguiente manera:

Creación de cada rama de cada desarrollador con su nombre propio asi:

- Developer-Fernanda: El cual incluye ejemplo de number y any del lenguaje typescript.
- Developer-Milena: El cual incluye ejemplo de string del lenguaje typescript y tambien el index.ts.
- Developer-Harol: El cual incluye ejemplo de array y boolean del lenguaje typescript.
- Developer-Carlos: El cual incluye ejemplo de array multidimensional del lenguaje typescript.

Luego se procede a subir cada archivo a su respectiva rama y siguiendo el flujo de subida asi: Developer-*** a Develop, ya al tener todas, Develop a release, y finalizando con release a master.